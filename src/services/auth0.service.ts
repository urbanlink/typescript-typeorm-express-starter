var ManagementClient = require('auth0').ManagementClient;
import { config } from './../config';
import logger from './../utils/logger';

class Auth0 {

  private auth0:any;
  // private auth0AuthClient: any;

  constructor() {
    this.auth0 = new ManagementClient({
      domain: config.auth0.domain,
      clientId: config.auth0.clientId,
      clientSecret: config.auth0.clientSecret,
      // scope: config.auth0.scope
    });
  }

  // Fetch users
  async getUsers(q: any = null): Promise<any> {
    logger.debug({ message: '[ auth0.service ] - Get users from auth0', params: q });
    return await this.auth0.getUsers(q);
  }

  //
  async hasRole(sub: string, role: string): Promise<boolean> {
    const profile = await this.auth0.getUser({id: sub});
    logger.info({ message: 'User received', profile: profile });
    if (!profile) { return false; }

    const roles = (profile.app_metadata || {}).roles || [];

    return (roles.indexOf(role) !== -1);
  }

  // Fetch current user
  async getUser(user_id: string): Promise<any> {
    logger.info(`Fetching user with id: ${user_id}`);
    return await this.auth0.getUser({ id: user_id });
  }
}

export default new Auth0;
