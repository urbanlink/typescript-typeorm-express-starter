import logger from './../utils/logger';
const Mollie = require('mollie-api-node');
const mollie = new Mollie.API.Client();
import { config } from './../config';
mollie.setApiKey(config.mollie.apikey);

// Fetch a payment from Mollie
export async function getPayment(id: string): Promise<any> {
  return new Promise((resolve, reject) => {
    mollie.payments.get(id, (result: any) => {
      logger.debug(result);
      if (result.error) {
        logger.debug('[ mollie.service ] - There was an error! ', result.error);
        reject(result.error);
      } else {
        resolve(result);
      }
    })
  })
}

// Create a new payment
export async function createPayment(data: any) {
  return new Promise((resolve, reject) => {
    mollie.payments.create(data, (result: any) => {
      logger.debug(result);
      if (result.error) {
        logger.debug('[ mollie.service ] - There was an error! ', result.error);
        reject(result.error);
      } else {
        resolve(result);
      }
    });
  });
}
