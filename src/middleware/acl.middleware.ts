import { NextFunction, Request, Response } from 'express';

// Validate if current user is admin
function isAdmin(req: Request, _res: Response, next: NextFunction) {

  let isAdmin: boolean = (req.user && req.user.admin);
  console.log(isAdmin);
  
  next();
}


export { isAdmin };
