import bodyParser from 'body-parser';
import express from 'express';
import Controller from './interfaces/controller.interface';
import errorMiddleware from './middleware/error.middleware';
import compression from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import jwt from 'express-jwt';
import jwks from 'jwks-rsa';
import { default as Cron } from './cron';

export default class App {

  private app: express.Application;
  private controllers: Controller[];

  constructor(_controllers: Controller[]) {
    this.controllers = _controllers;
  }

  async initialize(): Promise<boolean> {
    this.app = express();

    this.initializeMiddlewares();
    await this.initializeControllers(this.controllers);
    this.initializeErrorHandling();

    // initialize cron
    new Cron().init();

    return true;
  }

  listen() {
    this.app.listen(process.env.NODE_PORT, () => {
      console.log(`App listening on the port ${process.env.NODE_PORT}`);
    });
  }

  getServer() {
    return this.app;
  }

  // --- PRIVATE
  private initializeMiddlewares() {

    this.app.use(compression());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ limit: '5mb' }));

    // app.enable('trust proxy');
    this.app.set('json spaces', 2);

    // Use helmet to secure Express headers
    let SIX_MONTHS = 1000*60*60*24*31*6;
    this.app.use(helmet.frameguard());
    this.app.use(helmet.xssFilter());
    this.app.use(helmet.noSniff());
    this.app.use(helmet.ieNoOpen());
    this.app.use(helmet.hsts({maxAge: SIX_MONTHS, includeSubdomains: true, force: true}));
    this.app.use(helmet.noCache());
    this.app.disable('x-powered-by');

    this.app.use((_req: express.Request, res: express.Response, next: express.NextFunction) => {
      res.setHeader('X-Powered-By', 'De Haagse Munt - Powered by urbanlink.nl');
      next();
    });

    // 	// let corsOptions:any  ={};
    // 	// if (config.env === 'production') {
    // 	// 	corsOptions.origin = function(origin, callback) {
    // 	// 		const whitelist = [
    // 	// 			undefined', // ...
    // 	// 			'https://dashboard.dehaagsemunt.nl'
    // 	// 		];
    // 	// 		if (whitelist.indexOf(origin) === -1) {
    // 	// 			var msg = 'The CORS policy for this site does not allow access from the specified Origin: ' + origin;
    // 	// 			logger.debug(msg);
    // 	// 			return callback(msg, false);
    // 	// 		}
    // 	// 		return callback(null, true);
    // 	// 	}
    // 	// }
    this.app.use(cors({ maxAge: 1728000 }));

    // // Setup rate limiter
    // const limitOptions = {
    // 	windowMs: 60 * 1000,
    // 	max: 1000,
    // 	delayMs: 0,
    // }
    // app.use(new limit(limitOptions));

    let jwtCheck: any = jwt({
      secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: "https://dehaagsemunt.eu.auth0.com/.well-known/jwks.json"
      }),
      audience: 'api.dehaagsemunt.nl',
      issuer: "https://dehaagsemunt.eu.auth0.com/",
      algorithms: ['RS256']
    });

    // Use JWT
    this.app.use(jwtCheck.unless({
      path: [
        { url: '/', methods: [ 'GET' ] },                 // API Root
        { url: '/admin', methods: [ 'GET' ] },                 // API Root
        { url: '/admin/cron', methods: [ 'GET' ] },                 // API Root
        { url: '/payment/mollie', methods: [ 'POST' ] },  // Mollie payment webhook
      ]
    }));

    // initiate auth service
    require('./services/auth0.service');

  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private async initializeControllers(controllers: Controller[]) {
    console.log('controllers', controllers.length);
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }
}
