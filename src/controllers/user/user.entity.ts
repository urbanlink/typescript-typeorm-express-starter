import { Entity, Column, JoinTable, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, ManyToMany } from 'typeorm';
import { Length, IsEmail } from "class-validator";
import Todo from './../todo/todo.entity';


@Entity()
class User {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column({ unique: true })
  @Length(3, 50)
    auth0: string;

  @Column({ unique: true })
  @IsEmail()
    email: string;

  @Column({ unique: true })
  @Length(3, 25)
    username: string;

  @Column()
    picture: string

  @Column()
    plan: string

  @Column()
    type: string

  @Column({ default: false })
    admin: boolean

  @Column({ default: 'daily' })
    email_digest_schedule: string

  @Column({ nullable: true })
    last_email_digest: Date

  @CreateDateColumn({
      name: 'created_at',
      type: 'timestamptz'
    })
    createdAt: Date

  @OneToMany(() => Todo, todo => todo.user)
    todos: Todo[];


}

export default User;
