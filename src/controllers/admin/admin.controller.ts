import * as express from 'express';
import Controller from '../../interfaces/controller.interface';
import logger from './../../utils/logger';
import { isAdmin } from './../../middleware/acl.middleware';

export default class AdminController implements Controller {
  public path = '/admin';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, isAdmin, this.getStatus);
  }

  //
  private getStatus = async (_request: express.Request, response: express.Response, _next: express.NextFunction) => {
    logger.log('info', 'admin get status ');
    response.json(true);
  }
}
