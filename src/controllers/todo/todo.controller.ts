import * as express from 'express';
import { getRepository, getManager } from 'typeorm';
import Controller from '../../interfaces/controller.interface';
import Todo from './todo.entity';
import CreateTodoDto from './todo.dto';
import User from './../user/user.entity';
import logger from './../../utils/logger';

class TodoController implements Controller {

  entityManager = getManager();

  public path = '/todo';
  public router = express.Router();
  private todoRepository = getRepository(Todo);
  private userRepository = getRepository(User);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(this.path, this.createTodo);
    this.router.get(this.path, this.getTodos);
    this.router.get(`${this.path}/:id`, this.getTodo);
  }

  //
  private getTodos = async (req: express.Request, _res: express.Response, _next: express.NextFunction) => {

    logger.debug({ message: 'todo controller index ', user: req.user });

    return await getRepository(Todo).find();
  }

  //
  private getTodo = async (request: express.Request, response: express.Response, _next: express.NextFunction) => {

    logger.info('Show current todo for user ' + request.params.id);

    const todo = await this.todoRepository.findOne({
      where: { id: request.params.id }
    });

    response.json(todo);
  }

  //
  private createTodo = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {

    logger.debug({ message: '[ todo.controller ] - Create a new todo', data: req.body });

    let user: User;
    let todo: Todo;

    // initialize to user
    try {
      user = await this.userRepository.findOne({ where: { auth0: req.body.to } });
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }
    if (!user) { return res.status(404).json('User not found. '); }

    // Create todo data
    const todoData: CreateTodoDto = {
      description: req.body.description,
      user: user
    }


    try {
      todo = await this.todoRepository.create({ ...todoData });
      await this.todoRepository.save(todo);
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.send(todo);
  }
}

export default TodoController;
