import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Length } from "class-validator";
import User from './../user/user.entity';

@Entity()
class Todo {

  @PrimaryGeneratedColumn()
    id: number;

  @Column({ nullable: true, type: 'text' })
  @Length(3, 150)
    description: string;

  @Column({ default: false })
    processed: boolean

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
    createdAt: Date

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
    updatedAt: Date

  // Associations
  @ManyToOne(_type => User, user => user.todos, { eager: true })
  @JoinColumn({ name: 'user_id' })
    user: User;
}

export default Todo;
