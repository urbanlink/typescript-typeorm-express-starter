import { getRepository } from 'typeorm';
import Todo from './../todo/todo.entity';

// Fetch todos for a user
export async function getTodos(_sub: string): Promise<Todo[]> {

  return await getRepository(Todo).find();

}
