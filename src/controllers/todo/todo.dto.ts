import { IsNumber, IsString, Length, ValidateNested } from 'class-validator';
// import Todo from './todo.entity';

class UserInTodoDto {
  @IsNumber()
  id: number;
}

class CreateTodoDto {

  @IsString()
  @Length(3, 150)
  description?: string;

  @ValidateNested()
  user: UserInTodoDto;

}

export default CreateTodoDto;
