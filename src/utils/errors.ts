import * as express from 'express';
import * as Sentry from '@sentry/node';
import { config } from './../config';
import logger from './../utils/logger';

Sentry.init({
	dsn: config.sentry.dsn,
	environment: config.env,
	level: 'error',
});

// Create an error log if needed
function sendError(err: any, _req: express.Request, _res: express.Response, next: express.NextFunction) {
	var status =
		err.status ||
		err.statusCode ||
		err.status_code ||
		(err.output && err.output.statusCode) ||
		500;
	// skip anything not marked as an internal server error
	if (status < 500) return next(err);
	logger.error({ err });
	return next(err);
}

// Handle final: express.Application error response to user
function errorResponse(err: any, _req: express.Request, res: express.Response, _next: express.NextFunction) {
	switch (err.name) {

    case 'UnauthorizedError':
  		res.status(401).send('Missing authentication credentials.');
      break;

    // Error
    case 'Error':
      res.status(403).json({ msg: err.message })
      break;

    // Handle Page not Found
    case 'not_found':
      res.status(404).json({msg:'not_found'});
      break;

    // Handle Sequelize model validation errors
    case 'SequelizeValidationError':
    case 'SequelizeUniqueConstraintError':
      let e: string[] = [];
      err.errors.forEach((item: any) => {
        e.push(item.message);
      });
      res.status(403).json({
        errorType: 'validation_error',
        errors: e
      });
      break;

    default:
      res.status(403).json({msg: 'there was an error', err: err});
  }
}

// The request handler must be the first middleware on the app
export function	setupExpressRequestHandler(app: express.Application) {
	if (config.sentry.dsn) {
		app.use(Sentry.Handlers.requestHandler());
	}
};

// The error handler must be before any other error middleware
export function	setupExpressErrorHandler(app: express.Application) {
	// Use sentry if configured
	if (config.sentry.dsn) { app.use(Sentry.Handlers.errorHandler()); }
	// Log error so it is handled by sentry
	app.use(sendError);
  // send error to user
	app.use(errorResponse);
}

//
export function	Throw() {
	throw new Error('test');
}
