/*
{ error: 0,
  warn: 1,
  info: 2,
  http: 3,
  verbose: 4,
  debug: 5,
  silly: 6 }
*/

import { createLogger, format, transports } from 'winston';
import { config } from './../../config';
import { SentryWinstonTransport } from './sentry';

const logger = createLogger({
	level: config.logger.level,
	format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.errors({ stack: true }),
    format.splat(),
    format.json()
  ),
	transports: [
		new transports.File({
			level: 'warn',
			filename: 'logs/error.log',
			maxsize: 1024*1000*5,  // 5mb
			maxFiles: 20,
			tailable: true,
			zippedArchive: true
		}),
		new transports.File({
			level: 'info',
			filename: 'logs/info.log',
			maxsize: 1024*1000*5,  // 5mb
			maxFiles: 20,
			tailable: true,
			zippedArchive: true
		}),
		new transports.File({
			level: 'debug',
			filename: 'logs/debug.log',
			maxsize: 1024*1000*5,  // 5mb
			maxFiles: 20,
			tailable: true,
			zippedArchive: true,
			handleExceptions: true
		})
	]
});

// if (config.env !== 'production') {
	logger.add(new transports.Console({
    format: format.combine(
      format.colorize(),
      format.simple()
    )
  }));
// }

if (config.sentry.dsn) {
	logger.add(new SentryWinstonTransport({
		level: 'warn',
		dsn: config.sentry.dsn
	}));
}

export default logger;
