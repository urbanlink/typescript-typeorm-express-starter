export { fundSent} from './fund-sent.mailtemplate';
export { fundReceived } from './fund-received.mailtemplate';
export * from './payment-received.mailtemplate';
export * from './voucher-redeemed.mailtemplate';
export * from './voucher-received.mailtemplate';
export * from './watchdog.mailtemplate';
