import 'reflect-metadata';
import { bootstrap } from './bootstrap';
import App from './app';
import * as controllers from './controllers';
import Controller from 'interfaces/controller.interface';

bootstrap().then(async _connection => {

  let controllersArray: Controller[] = [];
  Object.keys(controllers).forEach(key => {
    controllersArray.push( new (<any>controllers)[ key]() );
  })

  const app = await new App(controllersArray);

  await app.initialize();

  app.listen();
}).catch(error => console.log('server error: ', error));
