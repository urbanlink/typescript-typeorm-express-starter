FROM keymetrics/pm2:8-alpine
MAINTAINER Arn van der Pluijm <https://gitlab.com/avdp>

EXPOSE 4000

WORKDIR /api/src

COPY package*.json ./
COPY ecosystem*.config.js ./
COPY tsconfig*.json ./

RUN npm install

COPY . .

CMD ./scripts/start.sh
